package com.epam.de.dsa;

public class ArrayWaveForm {

    public int[] arrangeElementsInWaveForm(int[] elements) {

        for (int i = 0; i < elements.length; i += 2) {
            if (i > 0 && elements[i] < elements[i - 1]) {
                int temp = elements[i];
                elements[i] = elements[i - 1];
                elements[i - 1] = temp;
            }

            if (i < elements.length - 1 && elements[i] < elements[i + 1]) {
                int temp = elements[i];
                elements[i] = elements[i + 1];
                elements[i + 1] = temp;
            }
        }
        return elements;
    }
}
