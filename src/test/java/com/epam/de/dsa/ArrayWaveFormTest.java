package com.epam.de.dsa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayWaveFormTest {
    ArrayWaveForm arrayWaveForm;
    @BeforeEach
    void setUp() {
        arrayWaveForm = new ArrayWaveForm();
    }
    @Test
    void arrangeElementsInWaveFormTest(){
        int[] arr = {20, 10, 8, 6, 4, 2};
        int[] expectedArr = {20, 8, 10, 4, 6, 2};
        assertArrayEquals(expectedArr,arrayWaveForm.arrangeElementsInWaveForm(arr));
    }
}